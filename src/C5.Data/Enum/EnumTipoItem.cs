﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C5.Data
{
    public enum EnumTipoItem
    {
        AlimentosNaoPereciveis = 1,
        EletroEletronicos = 2,
        InsumosHospitalares = 3
    }
}
