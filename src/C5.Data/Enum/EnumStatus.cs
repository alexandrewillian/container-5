﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C5.Data
{
    public class EnumStatus
    {
        public enum Status
        {
            Inativo = 0,
            Ativo = 1
        }
    }
}
