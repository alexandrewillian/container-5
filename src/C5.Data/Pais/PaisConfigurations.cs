﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace C5.Data
{
    public class PaisConfigurations : EntityTypeConfiguration<Pais>
    {
        public PaisConfigurations()
        {
            this.ToTable("Paises");
            this.HasKey(b => b.Id);
            this.Property(b => b.Id).HasColumnName("PaisId");
            this.Property(b => b.Nome).HasMaxLength(50);
            this.Ignore(b => b.Criado);
            this.Ignore(b => b.Modificado);
            this.Ignore(b => b.Status);
        }
    }
}
