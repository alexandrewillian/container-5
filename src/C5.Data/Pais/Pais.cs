﻿using System;
using System.Collections.Generic;

namespace C5.Data
{
    public class Pais : BaseAuditClass
    {
        public string Nome { get; set; }
        public Int16 Codigo { get; set; }

        public ICollection<Empresa> Empresa { get; set; }
    }
}
