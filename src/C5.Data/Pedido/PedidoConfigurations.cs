﻿using System.Data.Entity.ModelConfiguration;

namespace C5.Data
{
    public class PedidoConfigurations : EntityTypeConfiguration<Pedido>
    {
        public PedidoConfigurations()
        {
            this.ToTable("Pedidos");
            this.HasKey(b => b.Id);
            this.Property(b => b.Id).HasColumnName("PedidoId");
            this.Property(b => b.DataPedido).IsRequired()
                .HasColumnType("datetime2");
            this.Property(b => b.Peso).IsRequired();
            this.Property(b => b.DataEntrega).HasColumnType("datetime2");
            this.Property(b => b.DataTransporte).HasColumnType("datetime2");
            this.Property(b => b.Volumes).IsRequired();
            this.Property(b => b.Fragil).IsRequired();
            this.Property(b => b.Criado).HasColumnType("datetime2");
            this.Property(b => b.Modificado).HasColumnType("datetime2");
        }
    }
}
