﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace C5.Data
{
    public class Pedido : BaseAuditClass
    {
        public DateTime DataPedido { get; set; }
        public DateTime DataTransporte { get; set; }
        public DateTime DataEntrega { get; set; }
        public float Peso { get; set; }
        public Int16 Volumes { get; set; }
        public bool Fragil { get; set; }
        public string Observacao { get; set; }

        public Empresa Empresa { get; set; }
        public TipoItem TipoItem { get; set; }
    }
}
