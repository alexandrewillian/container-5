﻿using System;
using System.Collections.Generic;
using FluentValidation;

namespace C5.Data
{
    public class PedidoValidator : AbstractValidator<Pedido>, IPedidoValidator
    {
        public PedidoValidator()
        {
           
        }

        public bool VerficaTipoItem(List<Pedido> pedidos)
        {
            Pedido itemPedido = new Pedido();
            bool alimento = false;
            bool hospitalar = false;
            foreach (Pedido pedido in pedidos)
            {
                if (pedido.TipoItem.Equals(EnumTipoItem.AlimentosNaoPereciveis))
                    alimento = true;

                if (pedido.TipoItem.Equals(EnumTipoItem.InsumosHospitalares))
                    hospitalar = true;

                        if (alimento && hospitalar)
                    return (true);

            }
            return false;
        }
    }
}
