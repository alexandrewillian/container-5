﻿using System.Data.Entity.ModelConfiguration;

namespace C5.Data
{
    public class EmpresaConfigurations: EntityTypeConfiguration<Empresa>
    {
        public EmpresaConfigurations()
        {
            this.ToTable("Empresas");
            this.HasKey(b => b.Id);
            this.Property(b => b.Id).HasColumnName("EmpresaId");
            this.Property(b => b.NomeFantasia).HasMaxLength(50);
            this.Property(b => b.Cnpj).HasMaxLength(14);
            this.Property(b => b.RazaoSocial).HasMaxLength(50)
                .IsRequired();
            this.Property(b => b.Telefone).IsRequired();
            this.Property(b => b.Endereco).HasMaxLength(100);
            this.Property(b => b.Status).IsRequired();
            this.Property(b => b.Criado).HasColumnType("datetime2");
            this.Property(b => b.Modificado).HasColumnType("datetime2");
        }
    }
}
