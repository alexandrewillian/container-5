﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace C5.Data
{
    public class Empresa : BaseAuditClass
    {
        [Display(Name = "RazaoSocial", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "RazaoSocialObrigatorio")]
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Cnpj { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }

        public Pais Pais { get; set; }
        public ICollection<Pedido> Pedido { get; set; }
    }
}
