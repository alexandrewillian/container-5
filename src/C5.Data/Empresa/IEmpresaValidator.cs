﻿namespace C5.Data
{
    public interface IEmpresaValidator
    {
        bool ValidaCnpj(string text);
    }
}
