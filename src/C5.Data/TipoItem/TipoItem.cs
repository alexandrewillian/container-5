﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C5.Data
{
    public class TipoItem : BaseAuditClass
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }

        public ICollection<Pedido> Pedido { get; set; }
    }
}
