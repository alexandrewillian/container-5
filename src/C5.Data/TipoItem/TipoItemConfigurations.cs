﻿using System.Data.Entity.ModelConfiguration;

namespace C5.Data
{
    public class TipoItemConfigurations : EntityTypeConfiguration<TipoItem>
    {
        public TipoItemConfigurations()
        {
            this.ToTable("TipoItens");
            this.HasKey(b => b.Id);
            this.Property(b => b.Id).HasColumnName("TipoItemId");
            this.Property(b => b.Nome).HasMaxLength(50)
                .IsRequired();
            this.Property(b => b.Status).IsRequired();
            this.Ignore(b => b.Criado);
            this.Ignore(b => b.Modificado);
        }
    }
}
