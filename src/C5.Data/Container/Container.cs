﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C5.Data
{
    public class Container : BaseAuditClass
    {
        public string Nome { get; set; }
        public int PesoMinimo { get; set; }
        public int PesoMaximo { get; set; }
        public decimal PrecoPedido { get; set; }
    }
}
