﻿using System.Data.Entity.ModelConfiguration;

namespace C5.Data
{
    public class ContainerConfigurations : EntityTypeConfiguration<Container>
    {
        public ContainerConfigurations()
        {
            this.ToTable("Containers");
            this.HasKey(b => b.Id);
            this.Property(b => b.Id).HasColumnName("ContainerId");
            this.Property(b => b.Nome).HasMaxLength(15)
                .IsRequired();
            this.Property(b => b.Status).IsRequired();
            this.Property(b => b.Criado).HasColumnType("datetime2");
            this.Property(b => b.Modificado).HasColumnType("datetime2");
        }
    }
}
