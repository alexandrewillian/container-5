﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C5.Data
{
    public abstract class BaseAuditClass
    {
        public int Id { get; set; }
        public DateTime Criado { get; set; }
        public DateTime Modificado { get; set; }
        public byte Status { get; set; }
    }
}
