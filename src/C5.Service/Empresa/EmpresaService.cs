﻿using System;
using System.Collections.Generic;
using C5.Data;
using C5.Repo;

namespace C5.Service
{
    public class EmpresaService : IEmpresaService
    {
        private readonly IRepository<Empresa> _EmpresaRepository;
        public EmpresaService(IRepository<Empresa> EmpresaRepository)
        {
            _EmpresaRepository = EmpresaRepository;
        }

        public Data.Empresa GetEmpresa(int id)
        {
            return _EmpresaRepository.Get(id);
        }

        public IEnumerable<Empresa> GetEmpresas()
        {
            return _EmpresaRepository.GetAll();
        }

        public void InsertEmpresa(Empresa Empresa)
        {
            _EmpresaRepository.Insert(Empresa);
        }

        public void UpdateEmpresa(Empresa Empresa)
        {
            _EmpresaRepository.Update(Empresa);
        }

        public void DeleteEmpresa(int id)
        {
            var Empresa = _EmpresaRepository.Get(id);
            _EmpresaRepository.Delete(Empresa);
            _EmpresaRepository.SaveChanges();
        }
    }
}
