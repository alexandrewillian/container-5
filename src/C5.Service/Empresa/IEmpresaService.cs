﻿using System;
using C5.Data;
using System.Collections.Generic;

namespace C5.Service
{
    public interface IEmpresaService
    {
        IEnumerable<Empresa> GetEmpresas();
        Empresa GetEmpresa(int id);
        void InsertEmpresa(Empresa empresa);
        void UpdateEmpresa(Empresa empresa);
        void DeleteEmpresa(int id);
    }
}
