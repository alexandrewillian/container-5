namespace C5.Repo.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationZero : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Containers",
                c => new
                    {
                        ContainerId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 15),
                        PesoMinimo = c.Int(nullable: false),
                        PesoMaximo = c.Int(nullable: false),
                        PrecoPedido = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Criado = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificado = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Status = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ContainerId);
            
            CreateTable(
                "dbo.Empresas",
                c => new
                    {
                        EmpresaId = c.Int(nullable: false, identity: true),
                        RazaoSocial = c.String(nullable: false, maxLength: 50),
                        NomeFantasia = c.String(maxLength: 50),
                        Cnpj = c.String(maxLength: 14),
                        Email = c.String(),
                        Telefone = c.String(nullable: false),
                        Endereco = c.String(maxLength: 100),
                        Criado = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificado = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Status = c.Byte(nullable: false),
                        Pais_Id = c.Int(),
                    })
                .PrimaryKey(t => t.EmpresaId)
                .ForeignKey("dbo.Paises", t => t.Pais_Id)
                .Index(t => t.Pais_Id);
            
            CreateTable(
                "dbo.Paises",
                c => new
                    {
                        PaisId = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 50),
                        Codigo = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.PaisId);
            
            CreateTable(
                "dbo.Pedidos",
                c => new
                    {
                        PedidoId = c.Int(nullable: false, identity: true),
                        DataPedido = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataTransporte = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DataEntrega = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Peso = c.Single(nullable: false),
                        Volumes = c.Short(nullable: false),
                        Fragil = c.Boolean(nullable: false),
                        Observacao = c.String(),
                        Criado = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Modificado = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Status = c.Byte(nullable: false),
                        Empresa_Id = c.Int(),
                        TipoItem_Id = c.Int(),
                    })
                .PrimaryKey(t => t.PedidoId)
                .ForeignKey("dbo.Empresas", t => t.Empresa_Id)
                .ForeignKey("dbo.TipoItens", t => t.TipoItem_Id)
                .Index(t => t.Empresa_Id)
                .Index(t => t.TipoItem_Id);
            
            CreateTable(
                "dbo.TipoItens",
                c => new
                    {
                        TipoItemId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50),
                        Descricao = c.String(),
                        Status = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.TipoItemId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pedidos", "TipoItem_Id", "dbo.TipoItens");
            DropForeignKey("dbo.Pedidos", "Empresa_Id", "dbo.Empresas");
            DropForeignKey("dbo.Empresas", "Pais_Id", "dbo.Paises");
            DropIndex("dbo.Pedidos", new[] { "TipoItem_Id" });
            DropIndex("dbo.Pedidos", new[] { "Empresa_Id" });
            DropIndex("dbo.Empresas", new[] { "Pais_Id" });
            DropTable("dbo.TipoItens");
            DropTable("dbo.Pedidos");
            DropTable("dbo.Paises");
            DropTable("dbo.Empresas");
            DropTable("dbo.Containers");
        }
    }
}
