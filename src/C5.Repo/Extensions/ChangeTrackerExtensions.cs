﻿using System.Data.Entity.Infrastructure;
using System;
using C5.Data;
using System.Data.Entity;


namespace C5.Repo
{
    public static class ChangeTrackerExtensions
    {
        public static void ApplyAuditInformation(this DbChangeTracker changeTracker)
        {
            foreach (var entry in changeTracker.Entries())
            {
                if (!(entry.Entity is BaseAuditClass baseRegister)) continue;

                var now = DateTime.UtcNow;
                switch (entry.State)
                {
                    case EntityState.Modified:
                        baseRegister.Criado = now;
                        baseRegister.Modificado = now;
                        break;
                    case EntityState.Added:
                        baseRegister.Criado = now;
                        break;
                }
            }
        }
    }
}
