﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using C5.Data;

namespace C5.Repo
{
    public class DataContext : DbContext
    {
        public DataContext() : base("DefaultConnection")
        {
            //Database.SetInitializer<DataContext>(new C5DBInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region Carrega configurações

            modelBuilder.Configurations.Add(new EmpresaConfigurations());
            modelBuilder.Configurations.Add(new PaisConfigurations());
            modelBuilder.Configurations.Add(new ContainerConfigurations());
            modelBuilder.Configurations.Add(new PedidoConfigurations());
            modelBuilder.Configurations.Add(new TipoItemConfigurations());

            #endregion

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Pais> Paises { get; set; }
        public DbSet<Container> Containers { get; set; }
        public DbSet<Pedido> Pedido { get; set; }
        public DbSet<TipoItem> TipoItem { get; set; }
    }
}
