﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using C5.Repo;
using RazorPDF;

namespace C5.Web.Controllers
{
    public class RelatorioController : Controller
    {
        private DataContext db = new DataContext();

        // GET: Relatorio
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Relatorio()
        {
            var relatorio = db.Pedido.ToList();
            var result = new PdfResult(relatorio, "Index");
            result.ViewBag.Title = "Relatório de Pedidos";
            return View();
        }
    }
}