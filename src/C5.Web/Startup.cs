﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(C5.Web.Startup))]
namespace C5.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
